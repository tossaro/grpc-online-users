package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"

	proto "gitlab.com/tossaro/grpc-online-users/gen"
	"gitlab.com/tossaro/grpc-online-users/handler"
	"gitlab.com/tossaro/grpc-online-users/statshandler"
)

func main() {
	// Create a new gRPC server
	grpcServer := grpc.NewServer(grpc.StatsHandler(statshandler.New()))

	// Create a new connection pool
	var conn []*handler.Connection

	pool := &handler.Pool{
		Connection: conn,
	}
	go func() {
		for {
			pool.Cleansing()
			time.Sleep(time.Second * 3)
		}
	}()

	// Register the pool with the gRPC server
	proto.RegisterBroadcastServer(grpcServer, pool)

	// Create a TCP listener at port 8080
	listener, err := net.Listen("tcp", ":8080")

	if err != nil {
		log.Fatalf("Error creating the server %v", err)
	}

	fmt.Println("Server started at port :8080")

	// Start serving requests at port 8080
	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("Error creating the server %v", err)
	}
}
