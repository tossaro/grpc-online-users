# Real-Time User Online Checker

Real-Time User Online Checker Service using gRPC and Golang.

For running the service:
```
go build -o main &&  ./main
``