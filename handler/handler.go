package handler

import (
	"io"
	"log"
	"sync"
	"time"

	proto "gitlab.com/tossaro/grpc-online-users/gen"
)

type Connection struct {
	proto.UnimplementedBroadcastServer
	stream proto.Broadcast_CreateStreamServer
	user   proto.User
	error  chan error
}

type Pool struct {
	proto.UnimplementedBroadcastServer
	Connection []*Connection
}

func stringInSlice(id string, list []*Connection) bool {
	for _, c := range list {
		if c.user.Id == id {
			return true
		}
	}
	return false
}

func (p *Pool) Cleansing() {
	//log.Printf("start cleansing %v\n", len(p.Connection))
	for i, conn := range p.Connection {
		//log.Printf("try to reach %v\n", i)
		err := conn.stream.Send(&proto.User{})
		if err != nil {
			//log.Printf("removing index %v\n", i)
			p.Connection = append(p.Connection[:i], p.Connection[i+1:]...)
			conn.user.Active = false
			p.BroadcastUser(&conn.user)
			//log.Printf("current connected %v\n", len(p.Connection))
		}
	}
}

func (p *Pool) CreateStream(pstream proto.Broadcast_CreateStreamServer) error {
	ctx := pstream.Context()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		req, err := pstream.Recv()
		if err == io.EOF {
			go func() {
				time.Sleep(time.Second * 2)
				p.Cleansing()
			}()
			log.Println("exit")
			return nil
		}
		if err != nil {
			go func() {
				time.Sleep(time.Second * 2)
				p.Cleansing()
			}()
			log.Printf("receive error %v", err)
			continue
		}

		if req.Id == "" || stringInSlice(req.Id, p.Connection) {
			log.Printf("already online %v id = %v", req.Name, req.Id)
			continue
		}

		usr := proto.User{Id: req.Id, Name: req.Name, Avatar: req.Avatar, Active: true}
		conn := &Connection{
			stream: pstream,
			user:   usr,
			error:  make(chan error),
		}

		p.Connection = append(p.Connection, conn)
		for _, conn := range p.Connection {
			p.BroadcastUser(&conn.user)
		}
	}
}

func (p *Pool) BroadcastUser(u *proto.User) error {
	wait := sync.WaitGroup{}
	done := make(chan int)

	for _, conn := range p.Connection {
		wait.Add(1)
		go func(usr *proto.User, conn *Connection) {
			defer wait.Done()
			err := conn.stream.Send(usr)
			if err != nil {
				conn.error <- err
			}
		}(u, conn)
	}

	go func() {
		wait.Wait()
		close(done)
	}()

	<-done
	return nil
}
